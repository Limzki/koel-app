#!/usr/bin/env bash

# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

# shellcheck disable=SC2128
SOURCED=false && [ "$0" = "$BASH_SOURCE" ] || SOURCED=true

if ! $SOURCED; then
  set -eu
  trap 'echo "Error on line $LINENO."' ERR

  # Check for uninitialized variables
  set -o nounset

  # Check for command execution errors
  set -o errexit
fi

function usage() {
  echo "Usage: $0 -a <app> -f <path to file> -t <token>"
  echo "    -a    Specify the name of the app in the form <ownerName>/<appName>/<platform>"
  echo "    -f    Specify the path to the file to upload"
  echo "    -t    Specify the API token for the account"
  exit 1
}

while getopts ":a:f:t:" opt; do
  case $opt in
  a)
    APP="$OPTARG"
    ;;
  f)
    FILE_PATH="$OPTARG"
    ;;
  t)
    TOKEN="$OPTARG"
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    usage
    ;;
  :)
    echo "Option -$OPTARG requires an argument." >&2
    usage
    ;;
  esac
done

if [ -z "$APP" ] || [ -z "$FILE_PATH" ] || [ -z "$TOKEN" ]; then
  usage
fi

echo "Uploading $FILE_PATH to $APP..."

response=$(curl -X POST "https://api.appcenter.ms/v0.1/apps/$APP/release_uploads" \
  -H "accept: application/json" \
  -H "X-API-Token: $TOKEN" \
  -H "Content-Type: application/octet-stream" \
  --data-binary "@$FILE_PATH" \
  -o response.txt \
  -w "%{http_code}")

echo "$response"

if [ "$response" != "201" ]; then
  echo "Error uploading file"
  exit 1
else
  echo "File uploaded successfully"
fi

upload_id=$(cat response.txt | jq -r .upload_id)

echo "Upload ID: $upload_id"

echo "Committing release..."

response=$(curl -X POST "https://api.appcenter.ms/v0.1/apps/$APP/releases/1.0.0/upload_release" \
  -H "accept: application/json" \
  -H "X-API-Token: $TOKEN" \
  -H "Content-Type: application/json" \
  --data "{
    \"upload_id\": \"$upload_id\",
    \"status\": \"committed\"
  }" \
  -o response.txt \
  -w "%{http_code}")

echo "$response"

if [ "$response" != "200" ]; then
  echo "Error committing release"
  exit 1
else
  echo "Release committed successfully"
fi

rm response.txt
